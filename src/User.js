import React, { Component } from 'react';
import './App.css';
import {Link} from 'react-router';
class User extends Component {
  constructor(props) {
      super(props);
      this.state = {Users:[]};
      this.get_user=this.get_user.bind(this);
    }

    componentDidMount(){
    var url='https://infinite-savannah-85197.herokuapp.com/users';
   var self=this;
    fetch(url,{
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
    .then(async res => {
          const data = await res.json();
          self.setState({
            Users: data
          })
        })
        .catch(function(err) {
          console.log('ERROR!!! ' + err.message);
        });
}

get_user(id){
  const isuser = p => p._id === id
  return this.state.Users.find(isuser);
}
render() {
    var user = this.get_user(this.props.params.id  );
    var nm,imag,zon;
    for (var x in user) {
    nm= user.Nom;
    imag=user.Image;
    zon=user.Zone;
}
  return (

    <div>

     <div className="usertop"><h1>You are in the page of {nm}</h1></div>
     <div className="user">
    <h3>Nom :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{nm} </h3> <br/>
    <h3>Zone:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{zon} </h3>
    <img src={imag} className="image" />
    </div><br/><br/><br/><br/>
    <Link to="/"><h3>Home</h3></Link>
  </div>



  )}
}
export default User;
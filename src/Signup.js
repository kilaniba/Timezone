import React, { Component } from 'react';
import { withRouter,Link} from 'react-router';
//import {PlaceAutocomplete} from 'googleplaces';
import './signup.css';
var GooglePlaces = require('google-places');
var places = new GooglePlaces('AIzaSyDVKdbZbMwHGJu41-8424spJY5C2raJIho');
var moment = require('moment-timezone');
var a;
var latitude, longitude,Z;
const request = require('request');
class Signup extends Component {
    constructor(props) {
        super(props);
        this.state = {
          zone:'',
          //latitude:'',
          //longitude:'',
          //Z:''
        }
        this.Get_current_loc=this.Get_current_loc.bind(this);
        this.showPosition=this.showPosition.bind(this);
        this.Change_city=this.Change_city.bind(this);
        this.g=this.g.bind(this);
        this.get_lat_long=this.get_lat_long.bind(this);
        //this.get_timezoneid=this.get_timezoneid.bind(this);
        }
        componentDidMount(){
            var self=this;
           this.Get_current_loc();
           
        }
        Get_current_loc(){
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.showPosition);
        } else { 
            alert("Geolocation is not supported by this browser.");
        }
      }
      showPosition(position) {
        var self=this;
          
          fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&key=AIzaSyDuRhPaEq1QrBhsWHOrjQ5-MBIKB1fqyaU')
          .then(async res => {
                const data = await res.json();
                a=data.results[0].address_components[2].long_name;console.log(a);
                self.setState({
                  zone: a
                })
              })
              .catch(function(err) {
                console.log('ERROR!!! ' + err.message);
              });
      }
      Change_city(val){
        var self=this;
        var dataList = document.getElementById('json-datalist');
        var input = document.getElementById('ajax');
        this.setState({zone: val});
        /*places.autocomplete({input: self.state.zone, types: "(cities)"}, function(err, response) {
          console.log("autocomplete: ", response.predictions);
        
          var success = function(err, response) {
            //console.log("result: ", response.result);
            console.log("did you mean: ", response.result.name);
          };
        
          for(var index in response.predictions) {
            places.details({reference: response.predictions[index].reference}, success);
          }
          
        });*/
        var urauto='https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+self.state.zone+'&types=(cities)&key=AIzaSyDuRhPaEq1QrBhsWHOrjQ5-MBIKB1fqyaU'
        fetch(urauto)
        .then(async res => {
              const data = await res.json();
              //a=data.predictions[0].description;console.log(a);
              /*self.setState({
                zone: a
              })*/
              for(var i=0 ; i < data.predictions.length; i++){
                //console.log(data.predictions[i].description)
                var option = document.createElement('option');
                option.value =data.predictions[i].description;
                dataList.appendChild(option);
              }
            })
            .catch(function(err) {
              console.log('ERROR!!! ' + err.message);
            });
            this.setState({zone: val});
            console.log(val);
     this.get_lat_long(val);
      }
      get_lat_long(val){
        
        var self=this;
        var url_latlong='https://maps.googleapis.com/maps/api/geocode/json?address='+val+'&key=AIzaSyDR2tmC8dIu2iGl3I1IvExt-xdL2IW6Ujk'
        fetch(url_latlong)
        .then(async res => {
              const data = await res.json();
              //a=data.predictions[0].description;console.log(a);
              /*self.setState({
                zone: a
              })*/
              
              latitude=data.results[0].geometry.location.lat,
              longitude=data.results[0].geometry.location.lng         
              console.log(latitude,longitude);
              var url_tz='https://maps.googleapis.com/maps/api/timezone/json?location='+latitude+','+longitude+'&timestamp=1331766000&key=AIzaSyBU2Kk77r-o2bindARHEfCLdedanyd4z-4'
              fetch(url_tz)
              .then(async res => {
                    const data = await res.json();
                    var h=data.timeZoneId;console.log(h);                                      
                      document.getElementById("tz").innerHTML =h;                    
                  })
                  .catch(function(err) {
                    console.log('ERROR!!! ' + err.message);
                  });
            })
            .catch(function(err) {
              console.log('ERROR!!! ' + err.message);
            });
            
      }
      /*get_timezoneid(){
        console.log(latitude,longitude);
        var self=this;
        //console.log(latitude,longitude);
        var url_tz='https://maps.googleapis.com/maps/api/timezone/json?location='+self.state.latitude+','+self.state.longitude+'&timestamp=1331766000&key=AIzaSyBU2Kk77r-o2bindARHEfCLdedanyd4z-4'
        fetch(url_tz)
        .then(async res => {
              const data = await res.json();
              var h=data.timeZoneId;console.log(h);            
            })
            .catch(function(err) {
              console.log('ERROR!!! ' + err.message);
            });
      }*/
      g(){
        alert(Z)
      }
        render() {
    
          return (
    
            <div>
           <h1 >To sign up you must fill all fields</h1>    
            <form className="form_signup" >
            Username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" className="inputstyle"  /><br /><br /><br />
            Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" className="inputstyle"  /><br /><br /><br />
            Image:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="file" accept=".png, .jpg, .jpeg" className="inputstyle"  /><br /><br /><br />
            Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="password" className="inputstyle"  /><br /><br /><br />
            City:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input className="inputstyle" type="text" value={this.state.zone} onChange = {e => this.Change_city(e.target.value)} id="ajax" list="json-datalist" /><br />
            <datalist id="json-datalist"></datalist>
            <h5 className="timezone" id="tz"></h5><br /><br /><br />
            <input type="submit" value="Submit"/>
            </form>
            <Link to="/login"><h3>Log in</h3></Link>
    <button onClick={this.g}>llllll</button>
          </div>
    
    
    
          )}
        }
        
        export default withRouter(Signup);
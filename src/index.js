import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import User from './User';
import Login from'./Login';
import Username from './Username';
import Signup from './Signup';
import { Router, Route, browserHistory,IndexRedirect } from 'react-router';
import {requireAuth} from './Login';
ReactDOM.render(
    <Router history={browserHistory}>

      <Route path="/" component={App}>
      <IndexRedirect to="/timezone" /></Route>
      <Route path="/timezone" component={App} /*onEnter={requireAuth}*//>
      <Route path="/timezone/:id" component={User} />
      <Route path="/login" component={Login} />
      <Route path="/signup" component={Signup} />
      <Route path="/timezone/user/username" component={Username} /*onEnter={requireAuth}*//>
  </Router>, document.getElementById('root'));

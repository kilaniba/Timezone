import React, { Component } from 'react';
import './style_username.css';
import {Link} from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import { withRouter} from 'react-router';
var moment = require('moment-timezone');
var a,latitude, longitude;

var googleMapsClient = require('@google/maps').createClient({
  //key: 'AIzaSyDR2tmC8dIu2iGl3I1IvExt-xdL2IW6Ujk'
  //key:'AIzaSyDuRhPaEq1QrBhsWHOrjQ5-MBIKB1fqyaU'
  key:'AIzaSyBU2Kk77r-o2bindARHEfCLdedanyd4z-4'
});
const request = require('request');
const feathers = require('feathers/client');
const rest = require('feathers-rest/client');
const app = feathers();
const axios = require('axios');
const hooks = require('feathers-hooks');
const auth = require('feathers-authentication-client');
app.configure(hooks())
   .configure(rest('http://localhost:4200').axios(axios))
   .configure(auth({ storage: window.localStorage }));

class Username extends Component {
  constructor(props) {
      super(props);
      this.state = {
        zone:''
      }
      this.logout=this.logout.bind(this);
      this.Get_current_loc=this.Get_current_loc.bind(this);
      this.showPosition=this.showPosition.bind(this);
      this.Change_city=this.Change_city.bind(this);
      this.get_lat_long=this.get_lat_long.bind(this);
      }
     /* componentDidMount(){
      googleMapsClient.geocode({
        address: '1600 Amphitheatre Parkway, Mountain View, CA'
      }, function(err, response) {
        if (!err) {
          console.log(response.json.results);
        }
      });
      
      googleMapsClient.reverseGeocode({
        latlng: '36.8064,10.1815'
      }, function(err, response) {
        if (!err) {
          console.log(response.json.results);
        }
      });

      googleMapsClient.placesAutoComplete({
        input: 'p'
      }, function(err, response) {
        if (!err) {
          console.log(response.json.results);
        }
      });

      googleMapsClient.timezone({
        location: {latitude:"36.8064",longitude:"10.1815"}
      }, function(err, response) {
        if (!err) {
          console.log(response.json.results);
        }
      });
      }*/
    logout() {
    app.logout().then(()=>{this.props.router.push('/login')});
     }
  Get_current_loc(){
    var self=this;
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(this.showPosition);
      } else { 
          alert("Geolocation is not supported by this browser.");
      }
    }
  
   showPosition(position) {
    var self=this;
      console.log(position.coords.latitude,position.coords.longitude);
      fetch('https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+position.coords.longitude+'&key=AIzaSyDuRhPaEq1QrBhsWHOrjQ5-MBIKB1fqyaU')
      .then(async res => {
            const data = await res.json();
            a=data.results[0].address_components[2].long_name;console.log(a);
            self.setState({
              zone: a
            })
            var url_tz='https://maps.googleapis.com/maps/api/timezone/json?location='+position.coords.latitude+','+position.coords.longitude+'&timestamp=1331766000&key=AIzaSyBU2Kk77r-o2bindARHEfCLdedanyd4z-4'
            fetch(url_tz)
            .then(async res => {
                  const data = await res.json();
                  var h=data.timeZoneId;console.log(h);                                      
                    document.getElementById("tz").innerHTML =h;                    
                })
                .catch(function(err) {
                  console.log('ERROR!!! ' + err.message);
                });
          })
          .catch(function(err) {
            console.log('ERROR!!! ' + err.message);
          });
  }
    
  Change_city(val){
    var self=this;
    var dataList = document.getElementById('json-datalist');
    var input = document.getElementById('ajax');
    this.setState({zone: val});
    var urauto='https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+self.state.zone+'&types=(cities)&key=AIzaSyDuRhPaEq1QrBhsWHOrjQ5-MBIKB1fqyaU'
    fetch(urauto)
    .then(async res => {
          const data = await res.json();
          for(var i=0 ; i < data.predictions.length; i++){
            //console.log(data.predictions[i].description)
            var option = document.createElement('option');
            option.value =data.predictions[i].description;
            dataList.appendChild(option);
          }
        })
        .catch(function(err) {
          console.log('ERROR!!! ' + err.message);
        });
        this.setState({zone: val});
        console.log(val);
 this.get_lat_long(val);
  }
  get_lat_long(val){
    
    var self=this;
    var url_latlong='https://maps.googleapis.com/maps/api/geocode/json?address='+val+'&key=AIzaSyDR2tmC8dIu2iGl3I1IvExt-xdL2IW6Ujk'
    fetch(url_latlong)
    .then(async res => {
          const data = await res.json();
          latitude=data.results[0].geometry.location.lat,
          longitude=data.results[0].geometry.location.lng         
          console.log(latitude,longitude);
          var url_tz='https://maps.googleapis.com/maps/api/timezone/json?location='+latitude+','+longitude+'&timestamp=1331766000&key=AIzaSyBU2Kk77r-o2bindARHEfCLdedanyd4z-4'
          fetch(url_tz)
          .then(async res => {
                const data = await res.json();
                var h=data.timeZoneId;console.log(h);                                      
                  document.getElementById("tz").innerHTML =h;                    
              })
              .catch(function(err) {
                console.log('ERROR!!! ' + err.message);
              });
        })
        .catch(function(err) {
          console.log('ERROR!!! ' + err.message);
        });
        
  }
    render() {
        var user = this.props.params.username;

      return (

        <div>

        <form className="form_class" >
        Username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="text" className="inputstyle"  /><br /><br /><br />
        Name:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="text" className="inputstyle"  /><br /><br /><br />
        Image:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="file" accept=".png, .jpg, .jpeg" className="inputstyle"  /><br /><br /><br />
        Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input type="password" className="inputstyle"  /><br /><br /><br />
        City:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <input className="inputstyle" type="text" value={this.state.zone} onChange = {e => this.Change_city(e.target.value)} id="ajax" list="json-datalist" /><br />
        <datalist id="json-datalist"></datalist>
        <h5 className="timezone" id="tz"></h5><br /><br /><br />
        <input type="submit" value="Save"/>
        </form>
        <button className="button_location" onClick={this.Get_current_loc}>Use current location </button>
        <Link to="/timezone"><h3>Home</h3></Link>
      </div>



      )}
    }
    
    export default withRouter(Username);

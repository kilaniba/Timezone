import React, { Component } from 'react';
import './App.css';
import './style_username.css';
import {Link,withRouter} from 'react-router';
import Image from './Image'
var moment = require('moment-timezone');
var A=[];
var L;
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Users:[],
      Zonee:[]
    }
  }
  componentDidMount(){
    //Get our users from th server
    var url='https://infinite-savannah-85197.herokuapp.com/users';
   var self=this;
    fetch(url,{
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
    .then(async res => {
          const data = await res.json();
          self.setState({
            Users: data
          })
        })
        .catch(function(err) {
          console.log('ERROR!!! ' + err.message);
        });
        L=setInterval(function(){ document.getElementById("demo").innerHTML  =moment().format('hh:mm:ss a');},1000);
        
      }
      componentWillUnmount() {
        //clearInterval(L);
        for(var i = 0; i < A.length; i++){
          clearInterval(A[i]);
        }
      }
      Array_zone(T=[]){
        
              var t=[];
            for (var i = 0; i < T.length; i++) {
                if (!(t.includes(T[i].Zone)) ){
                    t.push(T[i].Zone)
                }
            }
            var S=t.sort(function(a,b) {return (moment.tz(a).hour() - moment.tz(b).hour()) });
            return S;
          }
          Create_div(z,i) {
            var V="a"+i;
            var t=[];
            var s={color:'blue'};
            var U=this.state.Users;
            for (var i = 0; i < U.length; i++) {
              if(U[i].Zone==z){t.push(U[i])}
            }
            V=setInterval(function(){document.getElementById(z).getElementsByTagName("div")[1].innerHTML =moment.tz( z).format('hh:mm:ss a');},1000);
            A.push(V);
              return(
                <div id={z} className="Column">{z}<br/>
                <div style={s}></div>
                {moment.tz( z).format('Z')} <br/>
                <div></div>
                 {
                                      t.map((p,i) => (
                                        this.Put_user(p.Nom,p.Zone,p.Image,p._id,p.idh)
                                  ))}
        
                </div>
              )
        
            }
            Put_user(nom,id_zone,src_image,id_image,id_h5){
              var o;
              if(moment.tz(id_zone).hour()>22 || moment.tz(id_zone).hour()<8 ){
                o="0.1";
                }
                if(moment.tz(id_zone).hour()>17 && moment.tz(id_zone).hour()<=22 ){
                o="0.5";
                }
                if(moment.tz(id_zone).hour()<=17 && moment.tz(id_zone).hour()>=8 ){
                o="1";
                }
              const textStyle = {
                color:'red',
                visibility: 'hidden',
              };
              const imgstyle={
                opacity:o
              }
                return(
                  <div>
                    <Link to={'/timezone/'+id_image} >
                    <Image source={src_image} imgstyle={imgstyle}/>
                    </Link>
               </div>
                )
          }
          Change_time(val){
            var t = this.state.Users;
          
             // set local Time
          
            var local=moment().hour();
            var verif = +val + +local;
            if (verif>23) {verif=verif - 24}
          
             //set Time zone
            for (var i = 0; i < t.length; i++) {
              var h=(moment.tz(t[i].Zone).hour());
              var v=+val + +h ;
          
              if(v>23){v= v - 24;}
              document.getElementById(t[i].Zone).getElementsByTagName("div")[0].innerHTML =moment.tz(t[i].Zone).hour(v).format('hh:mm:ss a');
          
              if (val==30){
                document.getElementById(t[i].Zone).getElementsByTagName("div")[0].innerHTML ="";
                if(moment.tz(t[i].Zone).hour()>22 || moment.tz(t[i].Zone).hour()<8 ){
                document.getElementById(t[i].Id_image).style.opacity="0.1";
                }
                if(moment.tz(t[i].Zone).hour()>17 && moment.tz(t[i].Zone).hour()<=22 ){
                  document.getElementById(t[i].Id_image).style.opacity="0.5";
                  }
                if(moment.tz(t[i].Zone).hour()<=17 && moment.tz(t[i].Zone).hour()>=8 ){
                    document.getElementById(t[i].Id_image).style.opacity="1";
                    }
                document.getElementById("rang").value=moment().hour();
            };
          
              if (val!=30){
                document.getElementById("demo").innerHTML  =moment().hour(verif).format('hh:mm:ss a');
                clearInterval(L);
                if(v>22 || v<8 ){
                document.getElementById(t[i].Id_image).style.opacity="0.1";
                }
                if(v>17 && v<=22 ){
                document.getElementById(t[i].Id_image).style.opacity="0.5";
                }
                if(v<=17 && v >=8 ){
                document.getElementById(t[i].Id_image).style.opacity="1";
                }
                  }
              }
            }
          
            // Function to stop our setinterval of local time
            Stop_time(){
                clearInterval(L);
                L=setInterval(function(){ document.getElementById("demo").innerHTML  =moment().format('hh:mm:ss a');},1000);
                this.Change_time("30");
          }
  
  render() {
    return (

    <div>
      <div id="demo" className="demo"></div><div className="range" >
       <input type="range" id="rang" min="0" max="23" step="1" defaultValue={moment().hour()} onChange={e => this.Change_time(e.target.value)} />
       <div><button onClick={this.Stop_time}> Now </button><button className="button" onClick={this.logout}><h3>Logout</h3></button>
       <Link to='/timezone/user/username' >
       <img src="../image/k.png" className='imgcount' />
       </Link>
       </div>
       </div>
      <div className="Row">

                         {this.Array_zone(this.state.Users).map((person,index) => (

                            this.Create_div(person,index))) }
        </div>
      
        </div>  );
}
}
export default withRouter(Login);
